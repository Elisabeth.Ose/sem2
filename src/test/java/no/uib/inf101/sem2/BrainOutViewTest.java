package no.uib.inf101.sem2;

import org.junit.jupiter.api.Test;

import BrainOut.model.GameModel;
import BrainOut.model.GameState;
import BrainOut.view.BrainOutView;

import java.awt.*;
import java.awt.event.MouseEvent;
import static org.junit.jupiter.api.Assertions.*;

public class BrainOutViewTest {
    
    // Test that the BrainOutView constructor initializes the view properly
    @Test
    public void testConstructor() {
        GameModel model = new GameModel();
        BrainOutView view = new BrainOutView(model);
        assertNotNull(view);
        assertNotNull(view.model);
        assertNotNull(view.CTheme);
        assertNotNull(view.currentScene);
        assertNotNull(view.gamestate);
        assertNull(view.icon);
        assertNotNull(view.redX);
       
    }
    
    // Test that the handelMouseClick method returns false when the click is not valid
    @Test
    public void testInvalidClick() {
        GameModel model = new GameModel();
        BrainOutView view = new BrainOutView(model);
        MouseEvent e = new MouseEvent(view, 0, 0, 0, 100, 100, 1, false);
        assertFalse(view.handelMouseClick(e));
    }
    

    // Test that the setGameState method sets the game state properly
    @Test
    public void testSetGameState() {
        GameModel model = new GameModel();
        BrainOutView view = new BrainOutView(model);
        GameState gameState = new GameState();
        view.setGameState(gameState);
        assertEquals(gameState, view.getGameState());
    }
    
    // Test that the clearIcon method sets the icon and redX list to null
    @Test
    public void testClearIcon() {
        GameModel model = new GameModel();
        BrainOutView view = new BrainOutView(model);
        view.icon = new Point(50, 50);
        view.redX.add(new int[]{50, 50});
        view.clearIcon();
        assertNull(view.icon);
        assertTrue(view.redX.isEmpty());
    }
}
