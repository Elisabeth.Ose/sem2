package no.uib.inf101.sem2;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import BrainOut.model.GameState;
import BrainOut.model.GameStatus;
import BrainOut.model.SceneFactory;
import BrainOut.model.Scenes.DarkestColorScene;
import BrainOut.model.Scenes.FullMarksScene;
import BrainOut.model.Scenes.RawEggScene;
import BrainOut.model.Scenes.RedButtonScene;
import BrainOut.model.Scenes.SceneType;
import BrainOut.model.Scenes.WelcomeScreen;
import BrainOut.view.BrainOutView;

public class SceneFactoryTest {

    private SceneFactory sceneFactory;
    private BrainOutView view;

    /**
     * Setup method to create a new SceneFactory and BrainOutView instance for each test.
     */
    @BeforeEach
    public void setup() {
        sceneFactory = SceneFactory.getInstance();
        view = new BrainOutView(null);
        sceneFactory.setView(view);
        GameState gameState = new GameState();
        sceneFactory = SceneFactory.getInstance();
        sceneFactory.setView(view);
    }

    /**
     * Test the getInstance method to ensure it creates a SceneFactory instance.
     */
    @Test
    public void testGetInstance() {
        assertNotNull(sceneFactory);
    }

    /**
     * Test the setView method to ensure it sets the view of the SceneFactory instance.
     */
    @Test
    public void testSetView() {
        assertNotNull(sceneFactory.setView(view));
    }
  
    /**
     * Test the singleton pattern by ensuring that the SceneFactory instance created by getInstance method is the same across two instances.
     */
    @Test
    void testSingleton() {
        SceneFactory instance1 = SceneFactory.getInstance();
        SceneFactory instance2 = SceneFactory.getInstance();
        Assertions.assertSame(instance1, instance2);
    }

    /**
     * Test the findNextSceneFromPrevious method by checking that it returns the correct SceneType for each previous SceneType.
     */
    @Test
    void testFindNextSceneFromPrevious() {
        Assertions.assertTrue(sceneFactory.findNextSceneFromPrevious(SceneType.WELCOME) instanceof DarkestColorScene);
        Assertions.assertTrue(sceneFactory.findNextSceneFromPrevious(SceneType.DARKETS_COLOR_SCENE) instanceof RawEggScene);
        Assertions.assertTrue(sceneFactory.findNextSceneFromPrevious(SceneType.RAW_EGG_SCENE) instanceof RedButtonScene);
        Assertions.assertTrue(sceneFactory.findNextSceneFromPrevious(SceneType.RED_BUTTON_SCENE) instanceof FullMarksScene);
        Assertions.assertTrue(sceneFactory.findNextSceneFromPrevious(SceneType.FULL_MARKS_SCENE) instanceof WelcomeScreen);
    }

    /**
     * Test the getNextScene method by checking that it returns the correct Scene for each GameStatus.
     */
    @Test
    void testGetNextScene() {
        GameState gameState1 = new GameState();
        gameState1.setGameStatus(GameStatus.INITIALIZING);
        Assertions.assertTrue(sceneFactory.getNextScene(gameState1) instanceof WelcomeScreen);

        GameState gameState2 = new GameState();
        gameState2.setGameStatus(GameStatus.ACTIVE);
        Assertions.assertTrue(sceneFactory.getNextScene(gameState2) instanceof DarkestColorScene);

        GameState gameState4 = new GameState();
        gameState4.setGameStatus(GameStatus.COMPLETED);
        Assertions.assertTrue(sceneFactory.getNextScene(gameState4) instanceof WelcomeScreen);
    }
}

