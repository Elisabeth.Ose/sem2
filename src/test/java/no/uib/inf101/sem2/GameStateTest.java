package no.uib.inf101.sem2;

import org.junit.jupiter.api.Test;

import BrainOut.model.GameState;
import BrainOut.model.GameStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameStateTest {

    /*tests if the function setGameStatus actually sets the gameStatus in game */
    @Test
    void testSetGameStatus() {
        GameState gameState = new GameState();
        gameState.setGameStatus(GameStatus.ACTIVE);
        assertEquals(GameStatus.ACTIVE, gameState.getGameStatus());
    }

    /*tests the getter that returns the gamestatus set in the setGamestatus */
    @Test
    void testGetGameStatus() {
        GameState gameState = new GameState();
        assertEquals(GameStatus.INITIALIZING, gameState.getGameStatus());
    }
    
}
