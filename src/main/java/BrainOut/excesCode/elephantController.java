package BrainOut.excesCode;

import java.awt.event.KeyEvent;

import BrainOut.model.GameStatus;
import BrainOut.model.Scenes.SceneType;
import BrainOut.view.BrainOutView;


/*this code is excess code that didn't work because my pc could not handle the mapping process of the labyrinth, but I wanted to keep
 * the code so I can look at it later and maybe try a different solution.
 */
public class elephantController implements java.awt.event.KeyListener{
        boolean inExit = false;
        BrainOutView view;
        ElephantScene scene = (ElephantScene) view.getCurrentScene();
    @Override
    public void keyPressed(KeyEvent e) {
    if (view.getCurrentScene().getSceneType() == SceneType.ELEPHANT_SCENE) {
        
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            inExit = scene.moveAndCheckElephant(-2, 0);

        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            inExit = scene.moveAndCheckElephant(2, 0);
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            inExit = scene.moveAndCheckElephant(0, 2);
        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            inExit = scene.moveAndCheckElephant(0, -2);
        }
        view.repaint();
        if (inExit) {
            view.getGameState().setGameStatus(GameStatus.TRANSITION);
        }
    }

}
    @Override
    public void keyReleased(KeyEvent arg0) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'keyReleased'");
    }
    @Override
    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'keyTyped'");
    }
}