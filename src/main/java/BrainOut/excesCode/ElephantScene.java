package BrainOut.excesCode;

/**

The ElephantScene class represents the scene in the game where the player has to navigate an
elephant through a maze to reach the exit. The class extends the TypablePanel class which allows
the player to move on to the next puzzle.
*/

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import BrainOut.model.GameStatus;
import BrainOut.model.TypablePanel;
import BrainOut.model.Scenes.SceneType;
import BrainOut.view.BrainOutView;
import BrainOut.view.ColorTheme;
import BrainOut.view.DefaultColorTheme;
import BrainOut.view.Inf101Graphics;

/*this code is excess code that didn't work because my pc could not handle the mapping process of the labyrinth, but I wanted to keep
 * the code so I can look at it later and maybe try a different solution.
 */

public class ElephantScene extends TypablePanel{

    private static final double OUTERMARGIN = 20;
    ColorTheme CT = new DefaultColorTheme();
    Point startingPosElephant = new Point(25, 218);
    Point currentPosElephant = startingPosElephant;
    Robot robot;
    BrainOutView view;
    Point exitPointTopLeft = new Point(220, 119);
    Point exitPointTopRight= new Point(246, 209);
    ArrayList<Point> labyrinthPoints; 
    Boolean labyrinthMapped = false;
    

    /**
     * Constructor for the ElephantScene class. Initializes the type attribute to SceneType.ELEPHANT_SCENE
     * and creates a Robot object used to capture the maze image. Initializes labyrinthPoints to an empty ArrayList.
     */
    public ElephantScene() {
        type = SceneType.ELEPHANT_SCENE;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.labyrinthPoints = new ArrayList<>();
    }

    /**
     * Draws the ElephantScene by drawing the maze, the elephant, and the text "Help the Elephant" and "Find the exit".
     * Calls the mapLabyrinth method if labyrinthMapped is false to map the maze.
     * @param g2 the Graphics2D object used to draw the scene
     */
    @Override
    public void drawScene(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        
        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "Help the Elephant", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 25);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "Find the exit", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 65);

        BufferedImage logo = Inf101Graphics.loadImageFromResources("/Labyrinth.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, logo, OUTERMARGIN*2, OUTERMARGIN*5.5, 0.25);
        drawElephant(g2);
        if (!labyrinthMapped) {
            mapLabyrinth();
        }
        
    }

    private void mapLabyrinth() {
        int x = (int)OUTERMARGIN * 2;
        int y = (int)(OUTERMARGIN * 5.5);
        int width = 859/4; 
        int height = 867/4;
        for (int i = x; i < x + width; i+=4) {
            for (int j = y; j < y + height; j+=4) {
                
                if (robot.getPixelColor(i, j).equals(Color.BLACK)) {
                    labyrinthPoints.add(new Point(i, j));
                    System.out.println(labyrinthPoints);
                }
            }
        }
        if (!labyrinthPoints.isEmpty()){
            labyrinthMapped = true;
        }
        System.out.println("labyrinth mapped");

    }

    /**

    Attempts to move the elephant by adding deltaX and deltaY to its current position.
    If the resulting position is valid, sets the elephant's current position to the new position.
    Returns true if the elephant is in the exit, false otherwise.
    @param deltaX the amount to add to the x-coordinate of the elephant's current position
    @param deltaY the amount to add to the y-coordinate of the elephant's current position
    @return true if the elephant is in the exit, false otherwise
    */
    public boolean moveAndCheckElephant(int deltaX, int deltaY) {
        Point newPosElephant = new Point((int)currentPosElephant.getX() + deltaX, (int)currentPosElephant.getY() + deltaY);
        
        if (isValidPos(newPosElephant)) {
            currentPosElephant = newPosElephant;
        }
        return elephantInExit(currentPosElephant);
    }

    /**
    Draws the elephant image at the elephant's current position on the screen.
    @param g2 the graphics object to draw on
    */
    private void drawElephant(Graphics2D g2) {
        BufferedImage logo = Inf101Graphics.loadImageFromResources("/Elephant.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, logo, currentPosElephant.getX(), currentPosElephant.getY(), 0.03);
    }

    /**
    Checks whether the given position is a valid position for the elephant to move to.
    Returns false if the position is within the boundaries of the labyrinth, and true otherwise.
    @param position the position to check for validity
    @return true if the position is valid, false otherwise
    */
    private boolean isValidPos(Point position) {
       Rectangle2D rect = new Rectangle(position, new Dimension(18, 20));
       for (Point point : labyrinthPoints) {
        if (rect.contains(point)) {
            return false;
        }
       }
       return true;
       
    }   

    /**
    Checks whether the given point is within the exit area.
    Returns true if the point is within the x and y range of the exit, and false otherwise.
    @param elephantPoint the point to check
    @return true if the point is within the exit, false otherwise
    */
    public boolean elephantInExit(Point elephantPoint) {
        int x1 = 220;
        int y1 = 119;
        int x2 = 246;
        int y2 = 209;
        
        // Check if point lies within the square's x and y range
        if (elephantPoint.x >= x1 && elephantPoint.x <= x2 && elephantPoint.y >= y1 && elephantPoint.y <= y2) {
            return true;
        } else {
            return false;
        }
    }



    @Override
    public boolean validateClick(MouseEvent e) {
        return false;
    }



    @Override
    public void drawTransition(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.1)));
        Inf101Graphics.drawCenteredString(g2, "Well done!", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
        g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
        Inf101Graphics.drawCenteredString(g2, "press any number ", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN);
        Inf101Graphics.drawCenteredString(g2, "to continue", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+20);
    }


    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        String regex = "[0-9]";
        boolean test = String.valueOf(e.getKeyChar()).matches(regex);
        if (view.getGameState().getGameStatus() == GameStatus.TRANSITION && test) {
            System.out.println(e.getKeyChar());
            return true;
        }
        return false;
    }

  
    
}
