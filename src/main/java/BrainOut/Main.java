package BrainOut;

import BrainOut.controller.BrainOutController;
import BrainOut.controller.MouseController;
import BrainOut.model.GameModel;
import BrainOut.view.BrainOutView;


import javax.swing.JFrame;

public class Main {
  
  public static void main(String[] args) {

    GameModel model = new GameModel();
    BrainOutView view = new BrainOutView(model);
    BrainOutController controller = new BrainOutController(model, view);
    MouseController mouseController = new MouseController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Brain Out");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
