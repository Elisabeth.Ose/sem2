package BrainOut.model.Scenes;

/**
This class represents the Darkest Color Scene in the game.
It extends TypablePanel and implements GameScene.
*/

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import BrainOut.model.GameStatus;
import BrainOut.model.TypablePanel;
import BrainOut.view.BrainOutView;
import BrainOut.view.ColorTheme;
import BrainOut.view.DefaultColorTheme;
import BrainOut.view.Inf101Graphics;

public class DarkestColorScene extends TypablePanel {

    /**
     * The constant value representing the margin of the scene.
     */
    private static final double OUTERMARGIN = 20;

    /**
     * The color theme object that is used in this scene.
     */
    ColorTheme CT = new DefaultColorTheme();

    /**
     * Constructor for DarkestColorScene class.
     */
    
    public DarkestColorScene() {
        type = SceneType.DARKETS_COLOR_SCENE;
    }


    @Override
    public void drawScene(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        
        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "Click to the darkest", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 25);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "color on the screen", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 65);

        BufferedImage logo = Inf101Graphics.loadImageFromResources("/purple_color_gradient.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, logo, OUTERMARGIN, OUTERMARGIN*3.5, 0.2);

    }

 

    @Override
    public boolean validateClick(MouseEvent e) {
        return e.getY() < 61;  
    }   

    @Override
    public void drawTransition(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
        Inf101Graphics.drawCenteredString(g2, "Well done, that's dark!", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
        g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
        Inf101Graphics.drawCenteredString(g2, "press any number ", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+20);
        Inf101Graphics.drawCenteredString(g2, "to continue", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+60);
    }


    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        String regex = "[0-9]";
        boolean test = String.valueOf(e.getKeyChar()).matches(regex);
        if (view.getGameState().getGameStatus() == GameStatus.TRANSITION && test) {
            System.out.println(e.getKeyChar());
            return true;
        }
        return false;
    }
}



    

