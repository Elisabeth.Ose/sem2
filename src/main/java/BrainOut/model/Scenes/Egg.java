package BrainOut.model.Scenes;

import java.awt.Point;

public class Egg {
    /**
     * The point of the egg object.
     */
    private Point point;

    /**
     * The state of the egg object (cracked or not).
     */
    private boolean cracked = false;

    /**
     * The path of the egg image file.
     */
    private String egg = "/Egg.png";

    /**
     * The path of the cracked egg image file.
     */
    private String crackedEgg = "/CrackedEgg.png";

    /**
     * The path of the cracked raw egg image file.
     */
    private String CrackedRawegg = "/CrackedRawEgg.png";

    /**
     * The state of the egg object (raw or not).
     */
    private boolean isRaw =false;

    /**
     * Returns the point of the egg object.
     *
     * @return the point of the egg object.
     */
    public Point getPoint() {
        return this.point;
    }

    /**
     * Sets the point of the egg object.
     *
     * @param point the point to be set.
     * @return the egg object with the new point.
     */
    public Egg setPoint(Point point) {
        this.point = point;
        return this;
    }

    /**
     * Returns the state of the egg object (cracked or not).
     *
     * @return true if the egg is cracked, false otherwise.
     */
    public boolean isCracked() {
        return this.cracked;
    }

    /**
     * Returns the state of the egg object (cracked or not).
     *
     * @return true if the egg is cracked, false otherwise.
     */
    public boolean getCracked() {
        return this.cracked;
    }

    /**
     * Sets the state of the egg object (cracked or not).
     *
     * @param cracked the state to be set (true for cracked, false for not cracked).
     * @return the egg object with the new state.
     */
    public Egg setCracked(boolean cracked) {
        this.cracked = cracked;
        return this;
    }

    /**
     * Returns the path of the egg image file.
     *
     * @return the path of the egg image file.
     */
    public String getFilePath() {
        if (cracked && isRaw) {
            return CrackedRawegg;
        }
        else if (cracked) {
            return crackedEgg;
        }
        else {
            return egg;
        }
    }

    /**
     * Returns the state of the egg object (raw or not).
     *
     * @return true if the egg is raw, false otherwise.
     */
    public boolean isIsRaw() {
        return this.isRaw;
    }

    /**
     * Returns the state of the egg object (raw or not).
     *
     * @return true if the egg is raw, false otherwise.
     */
    public boolean getIsRaw() {
        return this.isRaw;
    }

    /**
     * Sets the state of the egg object (raw or not).
     *
     * @param isRaw the state to be set (true for raw, false for not raw).
     * @return the egg object with the new state.
     */
    public Egg setIsRaw(boolean isRaw) {
        this.isRaw = isRaw;
        return this;
    }
}