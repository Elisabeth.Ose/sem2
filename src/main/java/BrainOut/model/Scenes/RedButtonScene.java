package BrainOut.model.Scenes;

/**
This class represents the Darkest Color Scene in the game.
It extends TypablePanel and implements GameScene.
*/

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.Robot;

import BrainOut.model.GameStatus;
import BrainOut.model.TypablePanel;
import BrainOut.view.BrainOutView;
import BrainOut.view.ColorTheme;
import BrainOut.view.DefaultColorTheme;
import BrainOut.view.Inf101Graphics;


public class RedButtonScene extends TypablePanel {

    private static boolean isTimeUp = false;
    boolean compleated = false;
    boolean doExplode = false;
    Timer timer = new Timer();
    BrainOutView view;
    int seconds = 20;
    Robot Robot;

    /**
     * The constant value representing the margin of the scene.
     */
    private static final double OUTERMARGIN = 20;

    /**
     * The color theme object that is used in this scene.
     */
    ColorTheme CT = new DefaultColorTheme();

    /**
     * Constructor for RedButtonScene class.
     */
    
    public RedButtonScene() {
        type = SceneType.RED_BUTTON_SCENE;
        countDown();
        try {
            this.Robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
    Returns the BrainOutView object associated with this RedButtonScene.
    @return the BrainOutView object associated with this RedButtonScene
    */
    public BrainOutView getView() {
        return this.view;
    }

    /**
    Sets the BrainOutView object associated with this RedButtonScene and returns this RedButtonScene.
    @param view the BrainOutView object to associate with this RedButtonScene
    @return this RedButtonScene with the specified view object set
    */
    public RedButtonScene setView(BrainOutView view) {
        this.view = view;
        return this;
    }




    @Override
    public void drawScene(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        
        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "DO NOT CLICK", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 25);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "THE RED BUTTON", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 65);

        BufferedImage earth = Inf101Graphics.loadImageFromResources("/Earth.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, earth, OUTERMARGIN*2.7, OUTERMARGIN*3.5, 0.15);
        BufferedImage button = Inf101Graphics.loadImageFromResources("/RedButton.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, button, this.getWidth()/2-2*OUTERMARGIN, OUTERMARGIN*13.2, 0.05);
        if (doExplode){
            seconds = 0;
            drawExplosion(g2);
            drawTryAgain(g2);
           
        }
        if (doExplode && isTimeUp){
            compleated = true;
            timer.cancel();
        }
        if (isTimeUp && !doExplode) {
            drawHappyEarth(g2);
            compleated = true;
            timer.cancel();
            
        }
    }
    /**
    Draws a "Try Again" button on the screen using the given Graphics2D object.
    @param g2 the Graphics2D object to use for drawing
    */
    private void drawTryAgain(Graphics2D g2) {
        BufferedImage button = Inf101Graphics.loadImageFromResources("/TryAgain.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, button, this.getWidth()/2-1.57*OUTERMARGIN, OUTERMARGIN*13.5, 0.055);
    }
   
    /**
    Handles the clicking of a button on the screen.
    If the clicked button is the "Try Again" button and the time has not run out,
    sets a flag to trigger an explosion animation and repaints the screen.
    @param e the MouseEvent object that triggered the button click
    */
    public void buttonClicked(MouseEvent e){
        if (e.getX() > 111 && e.getY() > 275 && e.getX() < 191 && e.getY() < 337) {
            if (!isTimeUp) {
                doExplode = true;
                repaint();
            }
        }
    }

    /**
    Draws a happy Earth image and a message on the screen using the given Graphics2D object.
    @param g2 the Graphics2D object to use for drawing
    */  
    private void drawHappyEarth(Graphics2D g2){
        BufferedImage HappyEarth = Inf101Graphics.loadImageFromResources("/HappyEarth.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, HappyEarth, OUTERMARGIN*2.7, OUTERMARGIN*3.5, 0.3);
        BufferedImage message = Inf101Graphics.loadImageFromResources("/patEarth.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, message, this.getWidth()/2-OUTERMARGIN*4.53, OUTERMARGIN*13.6, 0.75);
    }


    /**

    Draws an explosion image on the screen using the given Graphics2D object.
    @param g2 the Graphics2D object to use for drawing
    */
    private void drawExplosion(Graphics2D g2) {
        BufferedImage explosion = Inf101Graphics.loadImageFromResources("/Explosion.png");
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, explosion, OUTERMARGIN*2.7, OUTERMARGIN*3.5, 0.3);
    }


    /**
    Starts a timer that counts down from a specified time and sets a flag to indicate when the time is up.
    */
    public void countDown() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(seconds--);
                if (seconds < 0) {
                    isTimeUp = true;
                    if(isTimeUp){
                        view.repaint();
                    }
                }
            }
        }, 0, 1000); // delay of 0ms and period of 1s (1000ms)
    }



    @Override
    public boolean validateClick(MouseEvent e) {
        return compleated;
    }   

    @Override
    public void drawTransition(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);
        if(!doExplode){
            g2.setColor(CT.getBackgroundColor());
            g2.fill(rect1);
            g2.setColor(Color.BLACK);
            g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.1)));
            Inf101Graphics.drawCenteredString(g2, "BOOOORING!", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
            g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
            Inf101Graphics.drawCenteredString(g2, "press any number ", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+20);
            Inf101Graphics.drawCenteredString(g2, "to continue", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+50);
        }
        if (doExplode){
            g2.setColor(CT.getBackgroundColor());
            g2.fill(rect1);
            g2.setColor(Color.BLACK);
            g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.08)));
            Inf101Graphics.drawCenteredString(g2, "JUST KIDDING", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-4*OUTERMARGIN);
            Inf101Graphics.drawCenteredString(g2, "no seccond chance!", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
            g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
            Inf101Graphics.drawCenteredString(g2, "press any number ", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+20);
            Inf101Graphics.drawCenteredString(g2, "to continue", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+50);
        }

    }


    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        String regex = "[0-9]";
        boolean test = String.valueOf(e.getKeyChar()).matches(regex);
        if (view.getGameState().getGameStatus() == GameStatus.TRANSITION && test) {
            System.out.println(e.getKeyChar());
            return true;
        }
        return false;
    }
}



    

