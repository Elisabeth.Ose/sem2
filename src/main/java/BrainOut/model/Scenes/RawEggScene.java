package BrainOut.model.Scenes;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import BrainOut.model.GameStatus;
import BrainOut.model.TypablePanel;
import BrainOut.view.BrainOutView;
import BrainOut.view.ColorTheme;
import BrainOut.view.DefaultColorTheme;
import BrainOut.view.Inf101Graphics;


public class RawEggScene extends TypablePanel{

    private static final double OUTERMARGIN = 20;
    ColorTheme CT = new DefaultColorTheme();
    ArrayList<Egg> eggs;
    Egg egg1 =new Egg().setPoint(new Point(30, 110));
    Egg egg2 =new Egg().setPoint(new Point(120, 110));
    Egg egg3 =new Egg().setPoint(new Point(215, 110)).setIsRaw(true);
    Egg egg4 =new Egg().setPoint(new Point(30, 220));
    Egg egg5 =new Egg().setPoint(new Point(125, 220));
    Egg egg6 =new Egg().setPoint(new Point(215, 220));
    Boolean rawEggCrackedClicked = false;
    
    
     /**
     * constructor of the class Raw egg scene. 
     * Initializes the Raw Egg Scene by creating a list of eggs and adding them to the list.
     */
    public RawEggScene() {
        this.eggs = new ArrayList<>();
        type = SceneType.RAW_EGG_SCENE;
        eggs.add(egg1);
        eggs.add(egg2);
        eggs.add(egg3);
        eggs.add(egg4);
        eggs.add(egg5);
        eggs.add(egg6);
    }


     /**
     * Draws the Raw Egg Scene by setting the background color and filling a rectangle with that color. 
     * The method also sets the font, color, and text of the title and draws the eggs.
     * 
     * @param g2 - the graphics object used to draw the Raw Egg Scene
     */
    @Override
    public void drawScene(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        
        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.08)));
        Inf101Graphics.drawCenteredString(g2, "Find the Raw Egg", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, 25);
        for (int i = 0; i < eggs.size(); i++){
            drawEgg(g2, eggs.get(i));
        }
    }

    /**
     * Draws an egg at a specified point on the Raw Egg Scene.
     * 
     * @param g2 - the graphics object used to draw the egg
     * @param egg - the egg to be drawn
     */
    private void drawEgg(Graphics2D g2, Egg egg) {
        BufferedImage logo = Inf101Graphics.loadImageFromResources(egg.getFilePath());
        //double scale = (rect1.getWidth())/logo.getHeight();
        Inf101Graphics.drawImage(g2, logo, egg.getPoint().getX(), egg.getPoint().getY(), 0.06);
    }
    
    /**
    This method checks if the user has double-clicked on the mouse.
    @param e The MouseEvent that occurred.
    @return True if the user has double-clicked, False otherwise.
    */
    public boolean doubleMouseClick(MouseEvent e) {
        if (e.getClickCount() == 2) {
            return true;
        }
        return false;
    }


    /**
    This method validates a mouse click event and determines which egg was clicked.
    If an egg was clicked, it is set to cracked, and the method returns false.
    @param e The MouseEvent that occurred.
    @return True if the mouse click was not on an egg, False otherwise.
    */
    @Override
    public boolean validateClick(MouseEvent e) {
        if(doubleMouseClick(e) && 36 < e.getX() && e.getX() < 84 && 111 < e.getY() && e.getY() < 181){
            egg1.setCracked(true);
            return false;
        }
        if (doubleMouseClick(e) && 124 < e.getX() && e.getX() < 175 && 111 < e.getY() && e.getY() < 181){
            egg2.setCracked(true);
            return false;
        }
        if (doubleMouseClick(e) && 220 < e.getX() && e.getX() < 275 && 111 < e.getY() && e.getY() < 181){
            if (rawCrackedEgg() && !rawEggCrackedClicked) {
                return true;
            }
            else if (!rawCrackedEgg()) {
                rawEggCrackedClicked = true;
                egg3.setCracked(true);
            }
            else {
                rawEggCrackedClicked = false;
            }
            e.consume();
            return false;
        }
        if (doubleMouseClick(e) && 36 < e.getX() && e.getX() < 84 && 224 < e.getY() && e.getY() < 291){
            egg4.setCracked(true);
            return false;
        }
        if (doubleMouseClick(e) && 124 < e.getX() && e.getX() < 175 && 224 < e.getY() && e.getY() < 291){
            egg5.setCracked(true);
            return false;
        }
        if (doubleMouseClick(e) && 220 < e.getX() && e.getX() < 275 && 224 < e.getY() && e.getY() < 291){
            egg6.setCracked(true);
            return false;
        }
       
        
        return false;
      
    }  


    /**
    This method checks if there is a raw cracked egg and sets it to be clicked. If there is no
    raw cracked egg, it sets the egg that was clicked to cracked.
    @return True if there is a raw cracked egg, False otherwise.
    */
    private boolean rawCrackedEgg(){
        return eggs.stream().anyMatch(egg -> egg.getIsRaw() && egg.getCracked());
    }
    

    @Override
    public void drawTransition(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
        Inf101Graphics.drawCenteredString(g2, "Maybe you're the raw egg", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
        g2.setFont(new Font("Arial", Font.BOLD, (int)(this.getWidth()*0.07)));
        Inf101Graphics.drawCenteredString(g2, "press any number ", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+20);
        Inf101Graphics.drawCenteredString(g2, "to continue", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-OUTERMARGIN+60);
    }


    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        String regex = "[0-9]";
        boolean test = String.valueOf(e.getKeyChar()).matches(regex);
        if (view.getGameState().getGameStatus() == GameStatus.TRANSITION && test) {
            System.out.println(e.getKeyChar());
            return true;
        }
        return false;
    }
}



    


    

