package BrainOut.model.Scenes;

import java.awt.geom.Rectangle2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import BrainOut.model.GameStatus;
import BrainOut.model.TypablePanel;
import BrainOut.view.BrainOutView;
import BrainOut.view.ColorTheme;
import BrainOut.view.DefaultColorTheme;
import BrainOut.view.Inf101Graphics;

public class WelcomeScreen extends TypablePanel  {

    private static final double OUTERMARGIN = 20;
    public ColorTheme CT = new DefaultColorTheme();

    public WelcomeScreen() {
        type = SceneType.WELCOME;
    }

    @Override
    public void drawScene(Graphics2D g2) {
    
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1);

        
        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.1)));
        Inf101Graphics.drawCenteredString(g2, "WELCOME", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.05)));
        Inf101Graphics.drawCenteredString(g2, "press ANY key to play", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN+50);
    }


    @Override
    public boolean validateClick(MouseEvent e) {
        return false;
    }


    @Override
    public void drawTransition(Graphics2D g2) {
       
    }


    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        // From welcomescreen we ned to set gamestatus to active
        view.getGameState().setGameStatus(GameStatus.ACTIVE);
        return true;
    }
    
}
