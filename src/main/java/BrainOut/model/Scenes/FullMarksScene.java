package BrainOut.model.Scenes;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import BrainOut.model.GameStatus;
import BrainOut.model.TypablePanel;
import BrainOut.view.BrainOutView;
import BrainOut.view.ColorTheme;
import BrainOut.view.DefaultColorTheme;
import BrainOut.view.Inf101Graphics;

/**

FullMarksScene represents the scene where the user rates the game by choosing a grade.

Extends TypablePanel class.
*/
public class FullMarksScene extends TypablePanel {

    /** The outer margin of the scene. */
    private static final double OUTERMARGIN = 20;
    
    /** The color theme used for the scene. */
    ColorTheme CT = new DefaultColorTheme();
    
    /** A boolean indicating whether the correct answer has been selected or not. */
    Boolean correctAnswer = false;

    /*the timer that counts down when scene is almost compleat */
    Timer timer = new Timer();

    /*the amount of time the timer counts down */
    int seconds = 3;

    /*holds the scene value, if true it's compleate else false */
    boolean compleated = false;

    /*view of the game */
    BrainOutView view;

    /*checks if the timer is done counting */
    boolean isTimeUp = false;
    
    /**
    
    Constructs a new FullMarksScene object.
    Sets the type to SceneType.FULL_MARKS_SCENE.
    */
    public FullMarksScene() {
    type = SceneType.FULL_MARKS_SCENE;
    }

    /**
    Returns the BrainOutView object associated with this  FulleMarksScene
    @return the BrainOutView object associated with this  FulleMarksScene
    */
    public BrainOutView getView() {
        return this.view;
    }

    /**
    Sets the BrainOutView object associated with this FullMarksScene and returns this FullMarksScene.
    @param view the BrainOutView object to associate with this FullMarksScene
    @return this FulleMarksScene with the specified view object set
    */
    public FullMarksScene setView(BrainOutView view) {
        this.view = view;
        return this;
    }


    /**
    
    Draws the FullMarksScene on the given graphics object.
    
    @param g2 the graphics object to draw on.
    */
    @Override
    public void drawScene(Graphics2D g2) {
    
    g2.setBackground(getBackground());
    
    Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN,
    OUTERMARGIN,
    (this.getWidth() - 2 * OUTERMARGIN),
    (this.getHeight() - 2 * OUTERMARGIN));
    g2.setColor(getBackground());
    g2.fill(rect1);
    
    g2.setColor(CT.getBackgroundColor());
    g2.fill(rect1);
    g2.setColor(Color.BLACK);
    g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.07)));
    Inf101Graphics.drawCenteredString(g2, "What grade would you", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, OUTERMARGIN+30);
    Inf101Graphics.drawCenteredString(g2, "give this game?", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, OUTERMARGIN+70);
    Inf101Graphics.drawCenteredString(g2, "20%", OUTERMARGIN, OUTERMARGIN, OUTERMARGIN*3, OUTERMARGIN+150);
    Inf101Graphics.drawCenteredString(g2, "40%", OUTERMARGIN, OUTERMARGIN, OUTERMARGIN*3, OUTERMARGIN+270);
    Inf101Graphics.drawCenteredString(g2, "60%", OUTERMARGIN, OUTERMARGIN, OUTERMARGIN*3, OUTERMARGIN+390);
    Inf101Graphics.drawCenteredString(g2, "80%", OUTERMARGIN, OUTERMARGIN, OUTERMARGIN*3, OUTERMARGIN+510);
    Inf101Graphics.drawCenteredString(g2, "full marks", OUTERMARGIN, OUTERMARGIN, OUTERMARGIN*6, OUTERMARGIN+630);
    drawBoxes(g2);
  
    if (correctAnswer){
        drawGreenCheck(g2);
    }

    if (isTimeUp && correctAnswer){
            drawContinue(g2);
            compleated = true; 
            
        }
    }

    
    /*starts the timer countDown() */
    public void startTimer(){
        if (correctAnswer) {
            countDown();
        }
    }
    
    /**
    Draws the check boxes on the given graphics object.
    @param g2 the graphics object to draw on.
    */
    private void drawBoxes(Graphics2D g2) {
        BufferedImage logo = Inf101Graphics.loadImageFromResources("/CheckBox.png");
        Inf101Graphics.drawImage(g2, logo, this.getWidth()-3*OUTERMARGIN, OUTERMARGIN+70, 0.5);
        Inf101Graphics.drawImage(g2, logo, this.getWidth()-3*OUTERMARGIN, OUTERMARGIN+130, 0.5);
        Inf101Graphics.drawImage(g2, logo, this.getWidth()-3*OUTERMARGIN, OUTERMARGIN+190, 0.5);
        Inf101Graphics.drawImage(g2, logo, this.getWidth()-3*OUTERMARGIN, OUTERMARGIN+250, 0.5);
        Inf101Graphics.drawImage(g2, logo, this.getWidth()-3*OUTERMARGIN, OUTERMARGIN+310, 0.5);
        
    } 

    /**
    Draws a green check image on the screen.
    @param g2 the graphics object used to draw the image.
    */
    private void drawGreenCheck(Graphics2D g2) {
        BufferedImage logo = Inf101Graphics.loadImageFromResources("/GreenCheckBox.png");
        Inf101Graphics.drawImage(g2, logo, this.getWidth()-3*OUTERMARGIN, OUTERMARGIN+310, 0.5);
    }

    /**
    Draws a a pop up image on the screen.
    @param g2 the graphics object used to draw the image.
    */
    private void drawContinue(Graphics2D g2){
        BufferedImage logo = Inf101Graphics.loadImageFromResources("/ClickMe.png");
        Inf101Graphics.drawImage(g2, logo, OUTERMARGIN, OUTERMARGIN+17, 0.15);
       
    }

    /**
    Handles the clicking event of the check box. If the check box is clicked and the current answer is incorrect,
    sets the correct answer to true and repaints the scene.
    @param e the MouseEvent object containing information about the click event
    */
    public void checkBoxClicked(MouseEvent e){

        if (e.getX() > 245 && e.getX() < 265 && e.getY() > 333 && e.getY() < 355){
            if (!correctAnswer){
                correctAnswer = true;
                startTimer(); 
                repaint();
            }
        }
    }


    /**
    Starts the countdown timer. A TimerTask is scheduled to execute every 1000ms (1 second). The task decrements the
    value of seconds and checks if the value is less than 0. If so, the completed variable is set to true and the
    view is repainted.
    */
    public void countDown() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(seconds--);
                if (seconds < 0) {
                    isTimeUp = true;
                    view.repaint();
                    timer.cancel();
                    
                }
            }
        }, 0, 1000); // delay of 0ms and period of 1s (1000ms)
    }

    @Override
    public boolean validateClick(MouseEvent e) {
        return compleated;
    }


    @Override
    public void drawTransition(Graphics2D g2) {
        g2.setBackground(getBackground());
    
        Rectangle2D rect1 = new Rectangle2D.Double(OUTERMARGIN, 
        OUTERMARGIN, 
        (this.getWidth() - 2 * OUTERMARGIN), 
        (this.getHeight() - 2 * OUTERMARGIN));
        g2.setColor(getBackground());
        g2.fill(rect1); 

        
        g2.setColor(CT.getBackgroundColor());
        g2.fill(rect1);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.07)));
        Inf101Graphics.drawCenteredString(g2, "No that's really it", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
        g2.setFont(new Font("Comic Sans MS", Font.BOLD, (int)(this.getWidth()*0.05)));
        Inf101Graphics.drawCenteredString(g2, "press ANY key to play again", OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN+50);
    }


    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        // From welcomescreen we ned to set gamestatus to active
        view.getGameState().setGameStatus(GameStatus.ACTIVE);
        return true;
    }
    
}
