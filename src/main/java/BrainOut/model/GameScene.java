package BrainOut.model;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import BrainOut.view.BrainOutView;

    /**
    This interface defines the common methods required by all game scenes in the BrainOut game.
    A game scene represents a single screen or stage of the game.
    */

public interface GameScene {

    
    /**
     * Draws the current scene on the screen using the given graphics object.
     * 
     * @param g2 the graphics object to use for drawing
     */
    void drawScene(Graphics2D g2);

    /**
     * Validates whether a mouse click occurred within the valid bounds of the current scene.
     * 
     * @param e the mouse event that occurred
     * @return true if the mouse click was valid for the current scene, false otherwise
     */
    boolean validateClick(MouseEvent e);

    /**
     * Draws a transition from the current scene to the next scene.
     * 
     * @param g2 the graphics object to use for drawing the transition
     */
    void drawTransition(Graphics2D g2);

    /**
     * Validates whether a key event occurred within the valid bounds of the current scene.
     * 
     * @param e the key event that occurred
     * @param view the view object that will be updated based on the key event
     * @return true if the key event was valid for the current scene, false otherwise
     */
    boolean validateKey(KeyEvent e, BrainOutView view);

        
        
    }
