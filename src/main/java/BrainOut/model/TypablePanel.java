package BrainOut.model;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import BrainOut.model.Scenes.SceneType;
import BrainOut.view.BrainOutView;

public class TypablePanel extends JPanel implements GameScene{

    /**
     * The type of scene that this panel represents.
     */
    protected SceneType type;

    /**
     * Returns the type of scene that this panel represents.
     *
     * @return The SceneType representing the type of scene that this panel represents.
     */
    public SceneType getSceneType() {
        return type;
    }

    /**
     * Sets the size of this scene to the specified dimension.
     *
     * @param dimension The dimension to set the size of this panel to.
     * @return This TypablePanel object.
     */
    public TypablePanel setDimmension(Dimension dimension){
        return this;
    }

    /**
     * Draws the scene represented in the function.
     *
     * @param g2 The Graphics2D object used to draw the scene.
     */
    @Override
    public void drawScene(Graphics2D g2) {
        
    }


    /**
     * Determines whether a mouse click event is valid for the scene represented by this panel.
     *
     * @param e The MouseEvent object representing the mouse click event.
     * @return True if the mouse click event is valid for the scene represented by this panel, false otherwise.
     */
    @Override
    public boolean validateClick(MouseEvent e) {
    return false;
    }

    /**
     * Draws the transition screen between scenes.
     *
     * @param g2 The Graphics2D object used to draw the transition screen.
     */
    @Override
    public void drawTransition(Graphics2D g2) {
        throw new UnsupportedOperationException("Unimplemented method 'drawTransition'");
    }

    /**
     * Determines whether a key event is valid for the scene represented by this panel.
     *
     * @param e The KeyEvent object representing the key event.
     * @param view The BrainOutView object representing the game view.
     * @return True if the key event is valid for the scene represented by this panel, false otherwise.
     */
    @Override
    public boolean validateKey(KeyEvent e, BrainOutView view) {
        throw new UnsupportedOperationException("Unimplemented method 'validateKey'");
    }
}