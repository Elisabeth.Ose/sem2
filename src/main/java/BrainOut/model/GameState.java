package BrainOut.model;

/**
The GameState class represents the current state of the game.
It keeps track of the current game status, which can be one of several
GameStatus values.
*/
public class GameState {

    /**
    The current game status of the GameState object.
    */
    GameStatus gameStatus = GameStatus.INITIALIZING;

    /**
    Sets the game status of the GameState object.
    @param gameStatus the new game status to set
    @return the updated GameState object with the new game status
    */
    public GameState setGameStatus(GameStatus gameStatus) {
    this.gameStatus = gameStatus;
    return this;
    }

    /**
    Gets the current game status of the GameState object.
    @return the current game status of the GameState object
    */
    public GameStatus getGameStatus() {
    return gameStatus;
    }
    }
