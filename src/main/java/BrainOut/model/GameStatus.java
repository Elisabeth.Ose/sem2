package BrainOut.model;

public enum GameStatus {
    INITIALIZING, ACTIVE, TRANSITION, COMPLETED
}