package BrainOut.model;

/**
The SceneFactory class is responsible for creating new game scenes based on the current game state.
The scenes are implemented as TypablePanels, which are displayed to the user and provide an interactive experience.
This class implements the Singleton design pattern, ensuring that there is only one instance of the class throughout the program.
*/

import java.util.Objects;

import BrainOut.model.Scenes.DarkestColorScene;
import BrainOut.model.Scenes.FullMarksScene;
import BrainOut.model.Scenes.RawEggScene;
import BrainOut.model.Scenes.RedButtonScene;
import BrainOut.model.Scenes.SceneType;
import BrainOut.model.Scenes.WelcomeScreen;
import BrainOut.view.BrainOutView;

public class SceneFactory {
    
    BrainOutView view;
    private static SceneFactory instance;

    /**
     * Constructs a new SceneFactory object.
     * This is private because this class implements the Singleton design pattern.
     */
    private SceneFactory() {

    }

    /**
     * Sets the view associated with the SceneFactory.
     * @param view The BrainOutView associated with the SceneFactory.
     * @return The instance of the SceneFactory.
     */
    public SceneFactory setView(BrainOutView view) {
        this.view = view;
        return instance;
    }

    /**
     * Returns the singleton instance of the SceneFactory.
     * @return The singleton instance of the SceneFactory.
     */
    public static SceneFactory getInstance() {
        if (Objects.isNull(instance)){
            instance = new SceneFactory();
        }
        return instance;
    }

    /**
     * Given a SceneType, returns the next TypablePanel to display.
     * @param type The SceneType of the current scene.
     * @return The next TypablePanel to display.
     */
    public TypablePanel findNextSceneFromPrevious(SceneType type) {
        switch(type) {
            case WELCOME:
                return new DarkestColorScene();
            case DARKETS_COLOR_SCENE:
                return new RawEggScene();
            case RAW_EGG_SCENE:
                return new RedButtonScene().setView(view);
            case RED_BUTTON_SCENE:
                return new FullMarksScene().setView(view);
            case FULL_MARKS_SCENE:
                return new WelcomeScreen();
            default:
            return null;
        }
    }

    /**
     * Returns the next scene to display based on the current game state.
     * @param gameState The current GameState of the game.
     * @return The next TypablePanel to display.
     */
    public TypablePanel getNextScene(GameState gameState){

        TypablePanel gameScene = null;
        


        switch(gameState.gameStatus) {
            case INITIALIZING: 
                gameScene = new WelcomeScreen();
                break;
            case ACTIVE:
                gameScene = new DarkestColorScene();
                break;
            case TRANSITION:
                gameScene = findNextSceneFromPrevious(view.getCurrentScene().getSceneType());
                gameState.setGameStatus(GameStatus.ACTIVE);
                break;
            case COMPLETED:
                gameScene =  new WelcomeScreen();
                break;
            default:
                gameScene = null;
            
        }
        if (gameScene != null) {
            System.out.println(view.getPreferredSize());
            gameScene.setSize(view.getPreferredSize());
            return gameScene;
        }
        else {
            return null;       
        }     
    }
}
