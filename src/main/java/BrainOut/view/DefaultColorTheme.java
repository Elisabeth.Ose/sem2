package BrainOut.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {

   

    @Override
    public Color getFrameColor() {
        Color color = new Color(255, 171, 173);
        return color;
    }

    @Override
    public Color getBackgroundColor() {
        Color backgroundColor = new Color(255, 255, 255);
        return backgroundColor; 
    }

    @Override
    public Color getCompleatedBackgroundColor() {
        Color backgroundColor = new Color(255, 255, 255, (int) 0.5);
        return backgroundColor; 
    }
    
}
