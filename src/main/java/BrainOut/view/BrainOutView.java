package BrainOut.view;

/**
The BrainOutView class is responsible for handling the view of the game. It extends the JPanel class
and implements the graphical user interface for the game. It contains methods for handling mouse clicks
and for painting the game's scenes and transitions on the panel.
*/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

import BrainOut.model.GameModel;
import BrainOut.model.GameState;
import BrainOut.model.GameStatus;
import BrainOut.model.SceneFactory;
import BrainOut.model.TypablePanel;
import BrainOut.model.Scenes.SceneType;

import java.awt.Robot;


public class BrainOutView extends JPanel{

    public GameModel model;
    public ColorTheme CTheme;
    public TypablePanel currentScene;
    public GameState gamestate = new GameState();
    Robot robot;
    public ArrayList<int[]>  redX = new ArrayList<>();
    public Point icon = null;
    public Boolean doubleClick;
    

    /**
     * Constructs a new instance of BrainOutView with a specified GameModel.
     * @param model The GameModel to associate with the view
     */
    public BrainOutView(GameModel model) {
        
        this.model = model;
        this.CTheme = new DefaultColorTheme();
        this.setBackground(getBackground());
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(300, 400));
        this.currentScene = SceneFactory.getInstance().setView(this).getNextScene(gamestate);
    
    }

    /**
     * Handles a double mouse click event.
     * @param e The MouseEvent that was triggered
     */
    public void doubleClicked(MouseEvent e){
        if (e.getClickCount() == 2) {
            doubleClick = true;
        }
    }
        
    public void clearIcon(){
        icon = null;
        this.redX.clear();
    }
    
    /**
     * Handles a mouse click event.
     * @param e The MouseEvent that was triggered
     * @return True if the click was valid for the current scene, false otherwise
     */
    public boolean handelMouseClick(MouseEvent e) {
        boolean valid = false;

        if (currentScene.validateClick(e)) {
            icon = e.getPoint();
            valid = true;
        }
        else if (currentScene.getSceneType()== SceneType.DARKETS_COLOR_SCENE || currentScene.getSceneType() == SceneType.WELCOME || currentScene.getSceneType() == SceneType.RAW_EGG_SCENE) {
            if(redX.size() > 0 ){
                redX.clear();
            }
            int[] pos = {e.getX(),e.getY()};
            redX.add(pos);
        }
        repaint();
        return valid;

    }

    /**
     * Sets the current scene of the view.
     * @param scene The new current scene
     */
    public void setCurrentScene(TypablePanel scene) {
        this.currentScene = scene;
        paintComponent(getGraphics());
    }

    /**
     * Gets the current scene of the view.
     * @return The current scene
     */
    public TypablePanel getCurrentScene() {
        return currentScene;
    }

    /**
     * Sets the GameState of the game.
     * @param gameState The new GameState
     */
    public void setGameState(GameState gameState) {
        this.gamestate = gameState;
    }


    /**
     * Gets the current GameState of the game.
     * @return The current GameState
     */
    public GameState getGameState() {
        return gamestate;
    }

    /**
    Draws a red X at the specified location on the Graphics2D object.
    @param g2 the Graphics2D object to draw on
    @param x the x coordinate of the center of the X
    */
    private void drawRedX(Graphics2D g2, int x, int y) {
        g2.setColor(Color.RED);
        g2.drawLine(x-5, y-5, x+5, y+5);
        g2.drawLine(x-5, y+5, x+5, y-5);
        
    }

    /**
    Draws a green checkmark at the specified location on the Graphics2D object.
    @param g2 the Graphics2D object to draw on
    @param x the x coordinate of the center of the checkmark
    @param y the y coordinate of the center of the checkmark
    */
    private void drawGreenCheck(Graphics2D g2, int x, int y) {
        g2.setColor(Color.GREEN);
        g2.drawLine(x-5, y-5, x, y+2);
        g2.drawLine(x, y+2, x+10, y-7);
        
    }

    // The paintComponent method is called by the Java Swing framework every time
  // either the window opens or resizes, or we call .repaint() on this object. 
  // Note: NEVER call paintComponent directly yourself
  /**
   * paints the game by calling  drawgame
   * @param g Graphics
   */
     @Override
        public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        
        currentScene.drawScene(g2);
        for (int i = 0; i<redX.size(); i++) {
            int[] pos = redX.get(i);
            drawRedX(g2,pos[0], pos[1]);
        }
        if (icon != null) {
            drawGreenCheck(g2, (int)icon.getX(), (int)icon.getY());
           
        }
        if (getGameState().getGameStatus() == GameStatus.TRANSITION) {
            currentScene.drawTransition(g2);
            
        }
       
  }



}