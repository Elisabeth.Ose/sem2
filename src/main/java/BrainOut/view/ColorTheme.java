package BrainOut.view;
import java.awt.Color;

public interface ColorTheme {

    /**
     * sets the frame color of the board
     * 
     * @return the color of the frame
     */
    Color getFrameColor();

    /**
     * returns the color of the background of the board
     * 
     * @return background color
     */
    Color getBackgroundColor();

    
    /**
     * returns the color of the background of the board
     * when game is compleated
     * 
     * @return background color
     */
    Color getCompleatedBackgroundColor();


}
