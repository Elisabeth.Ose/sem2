package BrainOut.controller;

/**
The BrainOutController class is responsible for controlling the game based on user inputs
*/

import java.awt.event.KeyEvent;

import BrainOut.model.GameModel;
import BrainOut.model.SceneFactory;
import BrainOut.view.BrainOutView;



public class BrainOutController implements java.awt.event.KeyListener{

    GameModel model;
    BrainOutView view;

/**
 * Constructor for the BrainOutController class.
 * @param model The GameModel object associated with this controller
 * @param view The BrainOutView object associated with this controller
 */
public BrainOutController(GameModel model, BrainOutView view) {

    view.addKeyListener(this);

    this.model = model;
    this.view = view;
}

/**
 * Responds to user key press events by moving the elephant character on the game board if
 * the game state is type ELEPHANT_SCENE.
 * @param e The KeyEvent object containing information about the key press
 */
@Override
public void keyPressed(KeyEvent e) {

}

@Override
public void keyReleased(KeyEvent arg0) {

}


/**
 * Responds to user key typed events by checking if the key is valid and updating the current scene accordingly.
 * @param e The KeyEvent object containing information about the key typed
 */
@Override
public void keyTyped(KeyEvent e) {
    System.out.println(e.getKeyChar());
    if(view.getCurrentScene().validateKey(e, view)){
        view.setCurrentScene(SceneFactory.getInstance().getNextScene(view.getGameState()));
    }
}
}