package BrainOut.controller;

/**
MouseController class implements the MouseListener interface, handling mouse events
in the BrainOut game.
*/

import java.awt.event.MouseEvent;
import java.awt.AWTException;
import java.awt.Point;
import java.awt.Robot;
import BrainOut.model.GameModel;
import BrainOut.model.GameStatus;
import BrainOut.model.Scenes.FullMarksScene;
import BrainOut.model.Scenes.RedButtonScene;
import BrainOut.model.Scenes.SceneType;
import BrainOut.view.BrainOutView;

public class MouseController implements java.awt.event.MouseListener{

    GameModel model;
    BrainOutView view;
    Robot robot;
    GameStatus gameStatus;
    Point egg1 = new Point(30, 110);
    Point egg2 = new Point(120, 110);
    Point egg3 = new Point(215, 110);
    Point egg4 = new Point(30, 220);
    Point egg5 = new Point(125, 220);
    Point egg6 = new Point(215, 220);
      
   

    /**
 * Constructs a MouseController object with the specified GameModel and BrainOutView objects.
 * 
 * @param model the GameModel object used in the game.
 * @param view the BrainOutView object used to display the game.
 */
    public MouseController(GameModel model, BrainOutView view) {

        view.addMouseListener(this);
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        this.model = model;
        this.view = view;
    }

  

    @Override
    public void mouseClicked(MouseEvent e) {
       if (view.handelMouseClick(e) && view.getGameState().getGameStatus() == GameStatus.ACTIVE) {
        view.getGameState().setGameStatus(GameStatus.TRANSITION);
       }
       if (view.getCurrentScene().getSceneType() == SceneType.RAW_EGG_SCENE && view.handelMouseClick(e)) {
        view.getGameState().setGameStatus(GameStatus.TRANSITION);
       }
       if (view.getCurrentScene().getSceneType() == SceneType.RED_BUTTON_SCENE && view.handelMouseClick(e)) {
        view.getGameState().setGameStatus(GameStatus.TRANSITION);
       }
       if (view.getCurrentScene().getSceneType() == SceneType.RED_BUTTON_SCENE) {
        RedButtonScene redButton = (RedButtonScene)view.getCurrentScene();
        redButton.buttonClicked(e);
       }
       if (view.getCurrentScene().getSceneType() == SceneType.FULL_MARKS_SCENE) {
        FullMarksScene fullMarks = (FullMarksScene)view.getCurrentScene();
        fullMarks.checkBoxClicked(e);
       }
       if (view.getGameState().getGameStatus() == GameStatus.TRANSITION) {
        view.clearIcon();
       }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("mouse entered" + e.getX() + " " + e.getY());
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("mouse exited" + e.getX() + " " + e.getY());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("mouse pressed" + e.getX() + " " + e.getY());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("mouse released" + e.getX() + " " + e.getY());
    }

    
}
